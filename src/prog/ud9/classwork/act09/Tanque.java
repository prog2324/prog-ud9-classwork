package prog.ud9.classwork.act09;

/**
 *
 * @author sergio
 */
public class Tanque {

    private int capacidad;
    private int carga;

    public Tanque(int capacidad) {
        this.capacidad = capacidad;
        this.carga = 0;
    }

    public void agregarCarga(int carga) throws TanqueLlenoException {
        if ((this.carga + carga) > capacidad) {
            throw new TanqueLlenoException();
        }
        this.carga += carga;
    }

    public void retirarCarga(int carga) throws TanqueVacioException {
        if (this.estaVacio()) {
            throw new TanqueVacioException();
        }
        this.carga -= carga;
    }

    private boolean estaLleno() {
        return (carga == capacidad);
    }

    private boolean estaVacio() {
        return (carga == 0);
    }

}