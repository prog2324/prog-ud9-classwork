package prog.ud9.classwork.act10parelles.types;

public class Hippo extends Animal{

    public Hippo(Food food, Size size, String location) {
        super(food, size, location, "Hipopótamo");
    }

    @Override
    public void makeNoyse() {
        System.out.println("HIPPPP!!!!!!!!!");
    }
}
