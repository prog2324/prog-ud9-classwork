package prog.ud9.classwork;

/**
 *
 * @author sergio
 */
public class ActOpcparelles {

    public static void main(String[] args) {
        Integer[] array = {-2, -1, 0, 1, 2};

        dividirEntreArray(array, 2);
    }

    private static void dividirEntreArray(Integer[] array, int n) {
        for (Integer i : array) {
            try {
                if (i != 0) {
                    System.out.printf("%d / %d = %d\n", n, i, (n / i));
                }
            } catch (ArithmeticException e) {
                System.out.println("Error. No es posible realizar una división por cero");
            }
        }

    }
}
