package prog.ud9.classwork;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Act07 {

    public static void main(String[] args) {
        try {
            System.out.println("Hola. Bievenido");
            int edad = getEdad();
            System.out.printf("Tienes %d años\n", edad);
        } catch (InputMismatchException e) {
            System.err.println(e.getMessage());
        }
    }

    private static int getEdad() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Dime tu edad: ");
        int edad = sc.nextInt();
        if (edad < 10) {
            throw new InputMismatchException("La edad es demasiado baja");
        }
        if (edad > 50) {
            throw new InputMismatchException("La edad es demasiado alta");
        }
        return edad;
    }
}
