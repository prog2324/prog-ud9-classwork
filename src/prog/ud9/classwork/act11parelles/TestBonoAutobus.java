package prog.ud9.classwork.act11parelles;

import prog.ud9.classwork.act11parelles.exception.SaldoAgotadoException;

/**
 * Actividad11. Crea una clase BonoAutobus que represente un bono de 10 viajes para subir a un autobus metropolitano.
 * Esta clase dispondrá de una propiedad saldo que indique el número de viajes disponibles, y que serán establecidos
 * en el momento de su inicialización. Dispondrá de una metodo fichar el cual en caso de que no disponga de saldo, lanzará una excepción de tipo SaldoAgotadoException.
 * Crea una clase TestBonoAutobus que intente subir más de 10 veces al autobus.
 */
public class TestBonoAutobus {

    public static void main(String[] args) {

        BonoAutobus bono = new BonoAutobus();
        try {
            for (int i = 1; i <= 20; i++) {
                System.out.println("Voy a subir al autobus. Vez número " + i);
                bono.fichar();
                System.out.println(" -> He subido");
            }
        } catch (SaldoAgotadoException e) {
            System.out.println(e.getMessage() + ". Debes comprar otro bono.");
        }
    }

}

