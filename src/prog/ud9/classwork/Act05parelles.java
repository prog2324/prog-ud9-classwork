/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Act05parelles {

    public static void main(String[] args) {
        //caso valido
        int[] array3 = new int[3];
        readNumbers(array3, 3);

        //caso invalido
        readNumbers(array3, 4);

        //cas0 invalido
        int[] array = null;
        readNumbers(array, 5);

    }

    public static void readNumbers(int[] numbers, int size) {
        Scanner input = new Scanner(System.in);

        for (int i = 0; i < size; i++) {
            try {
                System.out.print("Introduce un número: ");
                numbers[i] = input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un número");
                input.nextLine();
                i--;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("El array ya está completo");
                return;
            } catch (NullPointerException e) {
                System.out.println("El array no esta inicializado");
                return;
            }

        }
    }

}
