package prog.ud9.classwork;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Act02 {

    public static void main(String[] args) {

        int max = 0;

        for (int i = 1; i <= 6;) {
            try {
                int n = obtenirEnter(i);
                max = (n > max)? n: max;
                i++;
            } catch (InputMismatchException e) {
                System.out.println("Error...");
            }
        }
        
        System.out.println("El mayor es: " + max);
    }

    public static int obtenirEnter(int i) {
        Scanner sc = new Scanner(System.in);
        System.out.printf("Introduce num %d: ", i);
        int n = sc.nextInt();
        return n;
    }
}
