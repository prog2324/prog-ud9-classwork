package prog.ud9.classwork.act11parelles.exception;

public class SaldoAgotadoException extends Exception {

    public SaldoAgotadoException(){
        super("No dispone de saldo para subir al autobus");
    }

}
