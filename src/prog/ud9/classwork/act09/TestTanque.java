package prog.ud9.classwork.act09;

/**
 *
 * @author sergio
 */
public class TestTanque {

    public static void main(String[] args) {
        Tanque tanque = new Tanque(1000);
        try {
            //System.out.println("Retirando...");
            //tanque.retirarCarga(50);
            tanque.agregarCarga(800);
            tanque.retirarCarga(50);
            tanque.agregarCarga(300);
        } catch (TanqueVacioException | TanqueLlenoException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
