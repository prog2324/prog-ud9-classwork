package prog.ud9.classwork.act06parelles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Act06 {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Estudiante 1");
        Student student1 = getNewStudent();
        System.out.println("Estudiante 2");
        Student student2 = getNewStudent();

        System.out.println(student1);
        System.out.println(student2);

        if (student1.compareTo(student2) > 0) {
            System.out.printf("El estudiante %s es mayor que el estudiante %s\n",
                    student1.getName(), student2.getName());
        } else if (student1.compareTo(student2) < 0) {
            System.out.printf("El estudiante %s es mayor que el estudiante %s\n",
                    student2.getName(), student1.getName());
        } else {
            System.out.println("Los dos tienen la misma edad");
        }
        ArrayList<Student> alumnosDam = new ArrayList<>();
        alumnosDam.add(student1);
        alumnosDam.add(student2);
        Collections.sort(alumnosDam);
        System.out.println(alumnosDam);
    }

    private static Student getNewStudent() {
        String name = getInputName();
        int age = getInputAge();
        float height = getInputHeight();
        return new Student(name, age, height);
    }

    private static String getInputName() {
//        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el nombre: ");
        return sc.nextLine();
    }

    private static int getInputAge() {
//        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print("Introduce la edad: ");
                return sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir una edad válida");
            } finally {
                sc.nextLine();
            }
        } while (true);
    }

    private static float getInputHeight() {
//        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print("Introduce la altura: ");
                return sc.nextFloat();
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir una altura válida");

            } finally {
                sc.nextLine();
            }

        } while (true);
    }

}
