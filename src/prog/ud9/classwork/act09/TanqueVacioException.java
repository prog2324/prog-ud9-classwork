package prog.ud9.classwork.act09;

/**
 *
 * @author sergio
 */
public class TanqueVacioException extends Exception {
    public TanqueVacioException() {
        super("Imposible retirar agua porque el tanque está vacío");
    }
}
