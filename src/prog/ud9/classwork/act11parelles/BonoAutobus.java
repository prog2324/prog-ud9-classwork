package prog.ud9.classwork.act11parelles;

import prog.ud9.classwork.act11parelles.exception.SaldoAgotadoException;



public class BonoAutobus {

    private final int DEFAULT_SALDO = 10;

    private int saldo;

    public BonoAutobus(){
        saldo = DEFAULT_SALDO;
    }

    public void fichar() throws SaldoAgotadoException {
        if (saldo == 0) {
            throw new SaldoAgotadoException();
        }
        saldo--;
    }

}


