package prog.ud9.classwork;

/**
 *
 * @author sergio
 */
public class Act08 {

    public static void main(String[] args) {
        System.out.println("Inicio programa...");
        try {
            waitSeconds(3);
        } catch (InterruptedException ex) {
            System.out.println("Error!! Esta excepción es explicita... " + ex.getMessage());
        }
        // catch (IllegalArgumentException ex) {
        //            System.out.println("Error!! Esta excepción es implicita... " + ex.getMessage());
        //        }

    }

    private static void waitSeconds(int seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000);
    }
}
