package prog.ud9.classwork;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Act03parelles {

    public static void main(String[] args) {
        String numerosIntroducidos = getNumbers();
        if (!numerosIntroducidos.isEmpty()) {
            System.out.println("Números introducidos: " + numerosIntroducidos);
        } else {
            System.out.println("No se han introducido números");
        }

    }

    public static String getNumbers() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder numIntro = new StringBuilder();
        do {
            System.out.print("Introduce un número: ");
            try {
                int number = scanner.nextInt();
                if (number >= 1 && number <= 5) {
                    return numIntro.toString();
                }
                if (!numIntro.isEmpty()) {
                    numIntro.append(",");
                }
                numIntro.append(number);
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un entero");
                scanner.nextLine();
            }
        } while (true);
    }

}
