package prog.ud9.classwork.act10parelles.exception;

public class ApareamientoInvalidoExcepcion extends Exception {

    public ApareamientoInvalidoExcepcion(){
        super("El apareamiento entre animales de distinta raza no es posible");
    }

    public ApareamientoInvalidoExcepcion(String msg){
        super(msg);
    }

}
