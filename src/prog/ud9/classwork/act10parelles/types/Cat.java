package prog.ud9.classwork.act10parelles.types;

public class Cat extends Animal{

    public Cat(Food food, Size size, String location) {
        super(food, size, location, "Gato");
    }

    @Override
    public void makeNoyse() {
        System.out.println("Miauuuu!!!!!!!!!");
    }
}
