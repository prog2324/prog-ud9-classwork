/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Act04parelles {
    public static void main(String[] args) {
        Integer max = getMaxNumber();
        if (max == null) {
            System.out.println("No se ha introducido ningún número");
        } else {
            System.out.println("El mayor de los números introducidos es " + max);
        }
    }
    
    public static Integer getMaxNumber(){
        Scanner input = new Scanner(System.in);
        Integer max = null;
        do {
            try {
                System.out.print("Introduce un número: ");
                int current = input.nextInt();
                if (max == null || current > max) {
                    max = current;
                }
            }catch (InputMismatchException e) {
                System.out.println("Debía haber introducido números enteros :-) ...");
                return max;
            }
        } while (true);
    }

}
