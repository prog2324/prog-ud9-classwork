package prog.ud9.classwork;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Act01 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int max = 0;

        for (int i = 1; i <= 6; ) {
            try {
                System.out.printf("Introduce num %d: ", i);
                int n = sc.nextInt();
                if (n > max) {
                    max = n;
                }
                i++;
            } catch (InputMismatchException e) {
                System.out.println("Error...");
                sc.next(); 
            }
        }
        System.out.println("El mayor es: " + max);
    }
}
