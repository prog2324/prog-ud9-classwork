package prog.ud9.classwork.act09;

/**
 *
 * @author sergio
 */
public class TanqueLlenoException extends Exception {
    public TanqueLlenoException() {
        super("Imposible agregar agua porque el tanque rebosará");
    }
}
